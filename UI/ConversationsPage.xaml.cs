﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextMessageApp
{
    /// <summary>
    /// Logique d'interaction pour ConversationsPage.xaml
    /// </summary>
    public partial class ConversationsPage : Page
    {
        // == Attributes ==
        private AppUser appUser;
        // ================

        public ConversationsPage()
        {
            InitializeComponent();

            // Initiate some objects for demo purpose
            initializeDemoApp();
        }

        void ChangeConversationLabel(object sender, MouseButtonEventArgs args)
        {
            var item = ItemsControl.ContainerFromElement(conversations_contacts_listbox, args.OriginalSource as DependencyObject) as ListBoxItem;

            if(item != null)
            {
                string name = item.Content.ToString();
                contact_name_label.Content = name;

                Contact cont = null;
                bool notFound = true;
                int index = 0;
                int listSize = appUser.AppUserContacts.Count;

                while(notFound && (index < listSize))
                {
                    if(appUser.AppUserContacts[index].ContactName.Equals(name))
                    {
                        cont = appUser.AppUserContacts[index];
                        notFound = false;
                    }

                    index++;
                }

                if(cont != null)
                {
                    conversations_listview.Items.Clear();
                    foreach(Message msg in cont.ContactMessages)
                    {
                        conversations_listview.Items.Add(msg.MessageContent);
                    }
                }
            }
        }

        // == DEMO ==
        private void initializeDemoApp()
        {
            // Creating AppUser
            this.appUser = new AppUser("Test");

            // Creating virtual contacts to fill the ListBox
            appUser.AddContact(new Contact("Alan Turing", "number"));
            appUser.AddContact(new Contact("Ada Lovelace", "number"));
            appUser.AddContact(new Contact("Claude Shannon", "number"));
            appUser.AddContact(new Contact("John von Neuman", "number"));
            appUser.AddContact(new Contact("George Boole", "number"));
            appUser.SortContacts();

            // Creating some messages
            foreach(Contact cont in appUser.AppUserContacts)
            {
                Random rnd = new Random();
                int msgNb = rnd.Next(1, 5);
                for(int i=0; i < msgNb; i++)
                {
                    Message msg = new Message("Random message " + i, cont, DateTime.Now);
                }
            }

            // Adding contacts to the list box
            foreach (Contact cont in appUser.AppUserContacts)
            {
                conversations_contacts_listbox.Items.Add(cont.ContactName);
            }
        }
        // ==========
    }
}
