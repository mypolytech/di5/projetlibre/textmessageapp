﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextMessageApp
{
    /// <summary>
    /// Logique d'interaction pour TextMessageHome.xaml
    /// </summary>
    public partial class TextMessageHome : Page
    {
        public TextMessageHome()
        {
            InitializeComponent();
        }

        private void ButtonConnexionClick(object sender, RoutedEventArgs e)
        {
            // View conversations page
            ConversationsPage conversationsPage = new ConversationsPage();
            this.NavigationService.Navigate(conversationsPage);
        }
    }
}
