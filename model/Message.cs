﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace TextMessageApp
{
    class Message : IComparable<Message>
    {
        // == Attributes ==
        public bool IsMessageRead { get; set; }
        public Contact MessageContact { get; set; }
        public DateTime MessageDateTime { get; set; }
        public string MessageContent { get; set; }
        public bool MessageIn { get; set; }
        // ================

        // == Constructors ==
        public Message(string content, Contact contact, DateTime dt)
        {
            this.MessageContent = content;
            this.MessageContact = contact;
            this.MessageDateTime = dt;
            this.IsMessageRead = false;
            this.MessageContact.addMessage(this);
        }

        public int CompareTo([AllowNull] Message other)
        {
            return this.MessageDateTime.CompareTo(other.MessageDateTime);
        }
        // ==================
    }
}
