﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace TextMessageApp
{
    class Contact : IComparable<Contact>, IEquatable<string>
    {
        // == Attributes ==
        private static string DEFAULT_URI = "resources/default_image.jpg";
        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactImageURI { get; set; }
        public List<Message> ContactMessages { get; set; }
        // ================

        // == Constructors ==
        public Contact(string name, string phoneNum)
        {
            this.ContactName = name;
            this.ContactPhoneNumber = phoneNum;
            this.ContactImageURI = Contact.DEFAULT_URI;
            this.ContactMessages = new List<Message>();
        }
        // ==================

        // == Methods ==
        public void addMessage(Message msg)
        {
            this.ContactMessages.Add(msg);
        }

        public int CompareTo([AllowNull] Contact other)
        {
            return this.ContactName.CompareTo(other.ContactName);
        }

        public bool Equals([AllowNull] string other)
        {
            return this.ContactName.Equals(other);
        }
        // =============
    }
}
