﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextMessageApp
{
    class AppUser
    {
        // == Attributes ==
        private static string DEFAULT_URI = "resources/default_image.jpg";
        public string AppUserPhone { get; set; }
        public string AppUserImage { get; set; }
        public List<Contact> AppUserContacts { get; set; }
        // ================

        // == Constructors ==
        public AppUser(string phoneNum)
        {
            this.AppUserPhone = phoneNum;
            this.AppUserImage = DEFAULT_URI;
            this.AppUserContacts = new List<Contact>();
        }
        // ==================

        // == Methods ==
        public void AddContact(Contact contact)
        {
            this.AppUserContacts.Add(contact);
        }

        public void SortContacts()
        {
            this.AppUserContacts.Sort();
        }
        // =============
    }
}
